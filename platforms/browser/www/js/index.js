/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
  // Application Constructor
  initialize: function() {
    this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicitly call 'app.receivedEvent(...);'
  onDeviceReady: function() {
    app.receivedEvent('deviceready');
    console.log("Device Ready");
    var url1 = 'https://www.con-conn.com/';
    var url = url1 + "&output=embed";
    var url = url1;    
//    url = 'https://apache.org';
    console.log("Loading: " + url);
    var ref = window.open(url, '_blank', 'location=yes');
    console.log("Con-conn");

//    var ref = window.open('http://apache.org', '_blank', 'location=yes');
  },
  // Update DOM on a Received Event
  receivedEvent: function(id) {
    var parentElement = document.getElementById(id);
    var listeningElement = parentElement.querySelector('.listening');
    var receivedElement = parentElement.querySelector('.received');

    listeningElement.setAttribute('style', 'display:none;');
    receivedElement.setAttribute('style', 'display:block;');

    console.log('Received Event: ' + id);

  },

  web_visitor: function() {
    console.log("visitor");
    var options = {
      location: 'no',
      clearcache: 'yes',
      toolbar: 'yes'
    };

    // Reader Mode set false

    console.log("ConConn");
    window.open('http://www.con-conn.com', '_blank', 'location=yes');
        //window.open('http://apache.org', '_blank', 'location=yes');

    //   Homer.web_visitor();

    //
    // function openUrl(url, readerMode) {
    //   SafariViewController.isAvailable(function(available) {
    //     if (available) {
    //       SafariViewController.show({
    //         url: url, hidden: false, // default false. You can use this to load cookies etc in the background (see issue #1 for details).
    //         animated: false, // default true, note that 'hide' will reuse this preference (the 'Done' button will always animate though)
    //         transition: 'curl', // (this only works in iOS 9.1/9.2 and lower) unless animated is false you can choose from: curl, flip, fade, slide (default)
    //         enterReaderModeIfAvailable: readerMode, // default false
    //         tintColor: "#00ffff", // default is ios blue
    //         barColor: "#0000ff", // on iOS 10+ you can change the background color as well
    //         controlTintColor: "#ffffff" // on iOS 10+ you can override the default tintColor
    //       },
    //       // this success handler will be invoked for the lifecycle events 'opened', 'loaded' and 'closed'
    //       function(result) {
    //         if (result.event === 'opened') {
    //           console.log('opened');
    //         } else if (result.event === 'loaded') {
    //           console.log('loaded');
    //         } else if (result.event === 'closed') {
    //           console.log('closed');
    //         }
    //       }, function(msg) {
    //         console.log("KO: " + msg);
    //       })
    //     } else {
    //       // potentially powered by InAppBrowser because that (currently) clobbers window.open
    //       window.open(url, '_blank', 'location=yes');
    //     }
    //   })
    // }
    //
    // function dismissSafari() {
    //   SafariViewController.hide()
    // }
  }
};
